var violationController = require('./Controllers/violationCtrl');

module.exports = function (app) {
    
    app.get('/', function (req, res) {
        res.send('Hello World!');
    })

    app.post('/logger', violationController.createViolation)

    app.get('/testcsp', function (req, res) {
        res.setHeader("Content-Security-Policy", "script-src 'self' https://apis.google.com; report-uri /logger");
        res.send('<html><body><script src="https://google.com"></script></body></html>');
    })

};